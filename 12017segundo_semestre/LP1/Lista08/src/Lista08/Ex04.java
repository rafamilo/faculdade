/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista08;

import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class Ex04 {
    public static int[] preencheVetor(int[] vetor){
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = i;
        }
        
        return vetor;
    }
    
    public static void printaVetor(int[] vetor){
        for (int i = 0; i < vetor.length ; i++) {
            System.out.print(vetor[i] + " ");
        }
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("Digite o tamanho do vetor");
        n = sc.nextInt();
        int[] vetor = new int[n];
        
        preencheVetor(vetor);
        printaVetor(vetor);
    }
}
