/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista08;

/**
 *
 * @author aluno
 */
public class Ex06 {
    public static int pegueoPombo (int x, int[] vetor){
        int numero = -1;
        
        for (int i = 0; i < vetor.length; i++) {
            if(i == x){
                numero = i;
            }
        }
        
        return numero;
    }
    
    public static void main(String[] args) {
        int[] vetor = new int[10];
        int x = 9;
        
        for (int i = 0; i < 10; i++) {
            vetor[i] = i;
        }
        
        System.out.println(pegueoPombo(x, vetor));
    }
}
