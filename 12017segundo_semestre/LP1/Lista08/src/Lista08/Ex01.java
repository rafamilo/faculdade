/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista08;

import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class Ex01 {

    public static double mediaQuatro(double numUm, double numDois, double numTres, double numQuatro) {
        return (numUm + numDois + numTres + numQuatro) / 4;
    }

    public static void main(String[] args) {
        double numUm, numDois, numTres, numQuatro, media;
        Scanner sc = new Scanner(System.in);
        numUm = sc.nextDouble();
        numDois = sc.nextDouble();
        numTres = sc.nextDouble();
        numQuatro = sc.nextDouble();
        
        media = mediaQuatro(numUm, numDois, numTres, numQuatro);
        System.out.println(media);
    }
}
