/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista08;

/**
 *
 * @author aluno
 */
public class Ex09 {

    public static int[] preencheVetor(int[] vetor) {
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = i;
        }

        return vetor;
    }

    public static void printaVetor(int[] vetor) {
        for (int i = 0; i < vetor.length; i++) {
            System.out.print(vetor[i] + " ");
        }
        System.out.println(" ");
    }


    public static int pegueoPombo(int x, int[] vetor) {
        int numero = -1;
        for (int i = 0; i < vetor.length; i++) {
            if (i == x) {
                numero = i;
            }
        }

        return numero;
    }

    //Foi utilizado o método de ordenação Bubble Sort
    public static void ordenaVetor(int[] vetor) {
        int n = vetor.length;
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (vetor[j - 1] > vetor[j]) {
                    temp = vetor[j - 1];
                    vetor[j - 1] = vetor[j];
                    vetor[j] = temp;
                }

            }
        }
    }
    
      public static int somaVetor(int[] vetor){
        int soma = 0;
        for (int i = 0; i < vetor.length ; i++) {
            soma += vetor[i];
        }
        return soma; 
    }
    

    public static void main(String[] args) {
        int[] vetor = new int[10];
        int numero = 5;
        preencheVetor(vetor);
        printaVetor(vetor);
        System.out.println(pegueoPombo(numero, vetor));
        System.out.println(somaVetor(vetor));
        ordenaVetor(vetor);
        printaVetor(vetor);
        
    }
}
