/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista08;

/**
 *
 * @author aluno
 */
public class Ex08 {
    public static int somaVetor(int[] vetor){
        int soma = 0;
        for (int i = 0; i < vetor.length ; i++) {
            soma += vetor[i];
        }
        return soma; 
    }
    
    public static void main(String[] args) {
        int[] vetor = new int[10];
        
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = i;
        }
        
        System.out.println(somaVetor(vetor));
    }
}
