/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista08;

import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class Ex02 {
    public static int fatorial(int numero){
        int total = 1;
        
        for (int i = 1; i <= numero; i++) {
            total *= i;
        }
        return total;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero;
        
        numero = sc.nextInt();
        System.out.println(fatorial(numero));
    }
}
