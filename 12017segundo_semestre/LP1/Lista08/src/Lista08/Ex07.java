/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista08;

import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class Ex07 {
    //Foi utilizado o método de ordenação Bubble Sort
    public static void ordenaVetor(int[] vetor) {
        int n = vetor.length;
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (vetor[j - 1] > vetor[j]) {
                    temp = vetor[j - 1];
                    vetor[j - 1] = vetor[j];
                    vetor[j] = temp;
                }

            }
        }
    }
    
    public static void main(String[] args) {
        int[] vetor = new int[5];
        Scanner sc = new Scanner(System.in);
        
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = sc.nextInt();
        }
        ordenaVetor(vetor);
        
        for (int i = 0; i < vetor.length; i++) {
            System.out.print(vetor[i] + " ");
        }
    }
}
