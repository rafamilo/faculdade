/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista08;

/**
 *
 * @author aluno
 */
public class Ex05 {
    public static void imprimeVetor(int[] vetor){
        for (int i = 0; i < vetor.length; i++) {
            System.out.print(vetor[i] + " ");
        }
    }
    
    public static void main(String[] args) {
        int[] vetor = new int[10];
        
        for (int i = 0; i < 10; i++) {
            vetor[i] = i;
        }
        
        imprimeVetor(vetor);
    }
}
