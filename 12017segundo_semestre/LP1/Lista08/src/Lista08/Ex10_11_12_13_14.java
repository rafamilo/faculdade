/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lista08;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class Ex10_11_12_13_14 {

    public static int[][] preencheMatriz(int[][] matriz) {

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matriz[i][j] = j;
            }
        }

        return matriz;
    }

    public static void imprimeMatriz(int[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println(" ");
        }
    }

    public static void leituraArquivo() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("entrada.txt"));

        while (sc.hasNext()) {
            String numero = sc.next();
            System.out.print(numero + " ");
        }
    }

    public static void preencheMatrizComLeituraArquivo(int[][] matriz) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("entrada.txt"));
         
        for(int i = 0 ; i < matriz.length; i++){
            for(int j = 0 ; j < matriz[i].length; j++){
                matriz[i][j] = sc.nextInt();
            }
        }
    }

    public static void main(String[] args) {
        int[][] matriz = new int[4][4];

        try {
            leituraArquivo();
        } catch (Exception e) {
            System.out.println("Deu ruim");
        }
        
        try {
            preencheMatrizComLeituraArquivo(matriz);
            imprimeMatriz(matriz);
        } catch (Exception e) {
            System.out.println("Deu ruim!");
        }

        preencheMatriz(matriz);
        imprimeMatriz(matriz);;
    }
}
