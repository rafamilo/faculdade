import java.util.Scanner;

public class Calculadora {

    public static double soma(double n1, double n2) {
        return n1 + n2;
    }

    public static double subtracao(double n1, double n2) {
        return n1 - n2;
    }

    public static double multiplicacao(double n1, double n2) {
        return n1 * n2;
    }

    public static double divisao(double n1, double n2) {
        return n1 / n2;
    }

    public static void menu() {
        System.out.println("Menu");
        System.out.println("1 - Soma");
        System.out.println("2 - Subtração");
        System.out.println("3 - Multiplicação");
        System.out.println("4 - Divisão");
        System.out.println("0 - Sair");
        System.out.print("Opção desejada: ");
    }

    public static int leituraOpcao() {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    public static double leituraNumero() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Entre com um número: ");
        return sc.nextDouble();
    }

    public static void mensagem(String msg) {
        System.out.println(msg);
    }

    public static void main(String[] args) {

        int op = 1;

        while (op != 0) {
            menu();
            op = leituraOpcao();

            switch (op) {
                case 1:
                    mensagem("Soma = " + soma(leituraNumero(), leituraNumero()));
                    break;
                case 2:
                    mensagem("Subtração = " + subtracao(leituraNumero(), leituraNumero()));
                    break;
                case 3:
                    mensagem("Multiplicação = " + multiplicacao(leituraNumero(), leituraNumero()));
                    break;
                case 4:
                    mensagem("Divisão = " + divisao(leituraNumero(), leituraNumero()));
                    break;
                case 0:
                    mensagem("Programa Finalizado");
                    break;
                default:
                    mensagem("Entrada Inválida");
                    break;

            }
        }

    }

}
