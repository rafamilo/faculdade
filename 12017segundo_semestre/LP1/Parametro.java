
//package aula5;   ajustar o packpage caso seja necessário

public class Parametro {

    public static void imprimi(String msg, String s, int x, double d, int v[], int m[][]) {

        System.out.println(msg);
        System.out.println("s = " + s);
        System.out.println("x = " + x);
        System.out.println("d = " + d);

        System.out.println("Vetor");
        for (int i = 0; i < v.length; i++) {
            System.out.print(v[i] + " ");
        }
        System.out.println("");

        System.out.println("Matriz");
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j] + " ");

            }
            System.out.println("");

        }
    }

    public static void altera(String s, int x, double d, int v[], int m[][]) {
        s = s + " Altera";
        x = 2;
        d = 2.2;
        for (int i = 0; i < v.length; i++) {
            v[i] = v[i] * 2;
        }

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                m[i][j] = m[i][j] * 2;
            }
        }
        
        imprimi("Valores dentro do método altera", s, x, d, v, m);
    }

    public static void main(String[] args) {
        String s = "Main";
        int x = 1;
        double d = 1.1;
        int v[] = {1, 2, 3, 4};
        int m[][] = {{1, 2, 3, 4}, {4, 5, 6, 7}, {8, 9, 10, 11}};

        imprimi("Valores Iniciais", s, x, d, v, m);
        altera(s, x, d, v, m);
        imprimi("Valores Na Main Após chamada do altera", s, x, d, v, m);
    }
}
